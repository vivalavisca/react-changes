import { CSSProperties, ReactNode } from "react";
// NO
console.log('in build');

declare module 'react-changes';

type Path = string;
type DefaultValue = any;

interface IState {
  [key: string]: any;
}

interface IVersion extends IState {
  _id: string;
}

interface IGlobalStateContext {
  globalState: IState;
  updateGlobalState(updatedState: IState, isUserChange?: boolean): void;
  getState(path: string): any;
  get: (path: string) => any;
  setState(path: string, value: any): void;
  set: (path: string, value: any) => void;
  undoState(): void;
  undo: () => void;
  redoState(): void;
  redo: () => void;
  clearHistory(): void;
  clear: () => void;
  getVersions(): IVersion[];
  versions: () => IVersion[];
  playHistory(ms?: number): void;
  play: (ms?: number) => void;
  loadVersion(id: string): void;
  load: (id: string) => void;
  seekVersion: (id: string) => void;
  seek: (id: string) => void;
  stopHistory(): void;
  stop: () => void;
}

declare const GlobalStateContext: React.Context<IGlobalStateContext | null>;

export const useGlobalState: () => IGlobalStateContext;

interface IGlobalStateProviderProps {
  children: React.ReactNode;
  debug?: boolean;
  controls?: boolean;
}

export const GlobalStateProvider: React.FC<IGlobalStateProviderProps>;

export function useChange(path: Path, defaultValue: DefaultValue): [any, (value: any) => void];
export function useChange(defaultValue: DefaultValue): [any, (value: any) => void];

export const useChangeControls: () => {
  undo: () => void;
  redo: () => void;
  play: (ms?: number) => void;
  stop: () => void;
  clear: () => void;
  versions: () => IVersion[];
  seek: (id: string) => void;
};

export const PlayerControls: React.FC;

export const Changes: React.FC<IGlobalStateProviderProps>;
