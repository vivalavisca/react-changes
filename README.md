# React-Changes

React-Changes is a versatile and easy-to-use state management library built for React applications. It simplifies the state management process by providing a `useChange` hook, a powerful custom hook for interacting with the global state. This hook can be used in a named or unnamed manner, and the library also provides versioning and playback capabilities for your global state changes.

## Features

- **useChange Hook**: This is a custom React hook that behaves like the built-in `useState` hook. It can be used in a named or unnamed manner. In both cases, it returns a pair, an array with the current state value and a function to update that value.

- **Nested State Access**: The named usage of `useChange` is particularly useful for accessing nested state properties. It's ideal for managing multi-page forms, more complicated data sets, or grouping related properties together.

- **Versioning**: Every change to the global state is stored as a unique version, making it possible to undo and redo changes, view the history of changes, or seek to a specific version.

- **Playback**: You can export the user's entire experience and reload it for playback. This feature is beneficial for running exact tests during QA (Quality Acceptance) or for quality assurance purposes.

## Installation

Use npm or yarn to install the package.

```shell
npm install react-changes
```

```shell
yarn add react-changes
```

## Usage

First, wrap your application in the `Changes` (GlobalStateProvider) component.

```jsx
import { Changes } from "react-changes";

function App() {
  return (
    <Changes>
      <YourComponent />
    </Changes>
  );
}
```

### Named usage

When you want to assign a specific name (path) to your state value, you can use the `useChange` hook as follows:

```jsx
import { useChange } from "react-changes";

function YourComponent() {
  const [value, setValue] = useChange("some.path", "default value");

  // To update the value
  setValue("new value");

  // ...
}
```

The named usage of `useChange` is particularly useful for accessing nested state properties. For example, you can use it to manage multi-page forms or group related properties together.

### Unnamed usage

To access a global state without providing a name (path), you can use the `useChange` hook as follows:

```jsx
import { useChange } from "react-changes";

function YourComponent() {
  const [value, setValue] = useChange("default value");

  // To update the value
  setValue("new value");

  // ...
}
```

In this case, the hook automatically assigns a unique, memoized string as the path to your state value.

To access the control actions for the global state, use the `useChangeControls` hook.

```jsx
import { useChangeControls } from "react-changes";

function YourComponent() {
  const { undo, redo, play, stop, clear, versions, seek } = useChangeControls();

  // ...
}
```

To display buttons for controlling the global state, use the `PlayerControls` component.

```jsx
import { PlayerControls } from "react-changes";

function YourComponent() {
  // ...
  return (
    <div>
      {/* Your component code... */}
      <PlayerControls />
    </div>
  );
}
```

## Debugging

For debugging, you can enable the `debug` prop in the `Changes` (GlobalStateProvider) component. This will expose the global state context to the window object.

```jsx
<Changes debug>
  {/* Your components */}
</
```
