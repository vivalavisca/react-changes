import React, {
  useState,
  createContext,
  useContext,
  useRef,
  CSSProperties,
} from "react";

interface IState {
  [key: string]: any;
}

interface IVersion extends IState {
  _id: string;
}

interface IGlobalStateContext {
  globalState: IState;
  updateGlobalState(updatedState: IState, isUserChange?: boolean): void;
  getState(path: string): any;
  get: (path: string) => any;
  setState(path: string, value: any): void;
  set: (path: string, value: any) => void;
  undoState(): void;
  undo: () => void;
  redoState(): void;
  redo: () => void;
  clearHistory(): void;
  clear: () => void;
  getVersions(): IVersion[];
  versions: () => IVersion[];
  playHistory(ms?: number): void;
  play: (ms?: number) => void;
  // DEPRECATING: loadVersion and load will be renamed to seekVersion and seek
  loadVersion(id: string): void;
  load: (id: string) => void;
  seekVersion: (id: string) => void;
  seek: (id: string) => void;
  stopHistory(): void;
  stop: () => void;
}

const GlobalStateContext = createContext<IGlobalStateContext | null>(null);

export const useGlobalState = (): IGlobalStateContext => {
  const context = useContext(GlobalStateContext);
  if (!context) {
    throw new Error("useGlobalState must be used within a GlobalStateProvider");
  }
  return context;
};

interface IGlobalStateProviderProps {
  children: React.ReactNode;
  debug?: boolean;
  controls?: boolean;
}

export const GlobalStateProvider: React.FC<IGlobalStateProviderProps> = ({
  children,
  debug = false,
  controls = false,
}) => {
  const [globalState, setGlobalState] = useState<IState>({});
  const [stateHistory, setStateHistory] = useState<IVersion[]>([]);
  const [currentVersion, setCurrentVersion] = useState<number>(0);

  const updateGlobalState = (
    updatedState: IState,
    isUserChange: boolean = true
  ) => {
    const newState: IState = { ...globalState, ...updatedState };
    setGlobalState(newState);

    if (isUserChange) {
      const newVersion: IVersion = {
        ...newState,
        _id: Math.random().toString(36).substring(7),
      };
      setStateHistory([
        ...stateHistory.slice(0, currentVersion + 1),
        newVersion,
      ]);
      setCurrentVersion(currentVersion + 1);
    }
  };

  const getState = (path: string) => {
    let value: any = globalState;
    path.split(".").forEach((key) => (value = value?.[key]));
    return value;
  };

  const setState = (path: string, value: any) => {
    const keys = path.split(".");
    const newGlobalState: IState = { ...globalState };
    let nestedObj: any = newGlobalState;

    keys.forEach((key, index) => {
      if (!nestedObj.hasOwnProperty(key) && index < keys.length - 1) {
        nestedObj[key] = {};
      }

      if (index === keys.length - 1) {
        nestedObj[key] = value;
      } else {
        nestedObj = nestedObj[key];
      }
    });

    updateGlobalState(newGlobalState);
  };

  const undoState = () => {
    if (currentVersion > 0) {
      setCurrentVersion(currentVersion - 1);
      setGlobalState(stateHistory[currentVersion - 1]);
    }
  };

  const redoState = () => {
    if (currentVersion < stateHistory.length - 1) {
      setCurrentVersion(currentVersion + 1);
      setGlobalState(stateHistory[currentVersion + 1]);
    }
  };

  const clearHistory = () => {
    setStateHistory([]);
    setCurrentVersion(0);
  };

  const isPlayingRef = useRef(false);
  const intervalRef = useRef<number | null>(null);

  const playHistory = (ms: number = 100) => {
    if (intervalRef.current !== null) {
      clearInterval(intervalRef.current);
    }

    let i = 0;
    isPlayingRef.current = true;
    intervalRef.current = setInterval(() => {
      if (isPlayingRef.current && i < stateHistory.length) {
        setGlobalState(stateHistory[i]);
        i++;
      } else {
        if (intervalRef.current) {
          clearInterval(intervalRef.current);
        }
        isPlayingRef.current = false;
      }
    }, ms);
  };

  const stopHistory = () => {
    isPlayingRef.current = false;
    if (intervalRef.current !== null) {
      clearInterval(intervalRef.current);
    }
  };

  const getVersions = (): IVersion[] =>
    stateHistory.map((state) => ({ ...state }));

  const seekVersion = (id: string) => {
    const versionIndex = stateHistory.findIndex((state) => state._id === id);
    if (versionIndex !== -1) {
      setCurrentVersion(versionIndex);
      setGlobalState(stateHistory[versionIndex]);
    }
  };

  const gs: IGlobalStateContext = {
    globalState,
    updateGlobalState,
    getState,
    get: getState,
    setState,
    set: setState,
    undoState,
    undo: undoState,
    redoState,
    redo: redoState,
    clearHistory,
    clear: clearHistory,
    getVersions,
    versions: getVersions,
    playHistory,
    play: playHistory,
    loadVersion: seekVersion,
    load: seekVersion,
    stopHistory,
    stop: stopHistory,
    seekVersion,
    seek: seekVersion,
  };

  if (debug) {
    (window as any).gs = gs;
  }

  return (
    <GlobalStateContext.Provider value={gs}>
      {children}
      {controls && <PlayerControls />}
    </GlobalStateContext.Provider>
  );
};

// useChange hook
// this hook is a wrapper for useGlobalState hook
// so that it can be used exactly like the useState hook
// instead of having to call useGlobalState().getState("count")
// and useGlobalState().setState("count", 1)
// you can just call useChange(1) and it will return an array like useState
// const [count, setCount] = useChange('count',1); // 1 is the default value
import { useMemo } from "react";

type Path = string;
type DefaultValue = any;

// Overloaded function signatures
export function useChange(path: Path, defaultValue: DefaultValue): [any, (value: any) => void];
export function useChange(defaultValue: DefaultValue): [any, (value: any) => void];
export function useChange(...args: [Path, DefaultValue] | [DefaultValue]): [any, (value: any) => void] {
  const { getState, setState } = useGlobalState();

  // Destructure the arguments to discern between the two function signatures
  const [path, defaultValue] = args.length === 2 ? args as [Path, DefaultValue] : [generateRandomString(), args[0]];

  const value = getState(path) || defaultValue;

  const updateValue = (newValue: any) => {
    setState(path, newValue);
  };
 
  return [value, updateValue];

  function generateRandomString() {
    return useMemo(() => {
      return '~' + Math.random().toString(36).substring(2, 15); // generates a random string
    }, []); // Dependencies array is empty which means the function will only be invoked on the initial render
  }
};



export const useChangeControls = () => {
  const { undo, redo, play, stop, clear, versions, seek } = useGlobalState();
  return {
    undo,
    redo,
    play,
    stop,
    clear,
    versions,
    seek,
  };
};

export const PlayerControls = () => {
  const { undo, redo, play, stop, clear, versions } = useGlobalState();
  const styles: {
    container: CSSProperties;
    action: CSSProperties;
  } = {
    container: {
      backgroundColor: "#333",
      position: "fixed",
      bottom: 0,
      left: 0,
      width: "100vw",
      display: "flex",
    },
    action: {
      backgroundColor: "transparent",
    },
  };
  return (
    <div style={styles.container}>
      <button style={styles.action} onClick={() => undo()}>
        ↩️ Undo
      </button>
      <button style={styles.action} onClick={() => redo()}>
        <span style={{ transform: "scaleX(-1)" }}>↩️</span> Redo
      </button>
      <button style={styles.action} onClick={() => play()}>
        ▶️ Play
      </button>
      <button style={styles.action} onClick={() => stop()}>
        ⏹️ Stop
      </button>
      <button style={styles.action} onClick={() => clear()}>
        🚮 Clear
      </button>
      <button style={styles.action} onClick={() => console.log(versions())}>
        📁 Versions
      </button>
    </div>
  );
};

export const Changes = GlobalStateProvider;

console.log("React-Changes Has Loaded");
