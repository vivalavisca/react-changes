import { defineConfig } from "vite";
import path, { dirname } from 'path'
import react from "@vitejs/plugin-react";
import { fileURLToPath } from 'url';
import dts from "vite-plugin-dts";


const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react() ,dts({
    insertTypesEntry: true,
  })],
  build: {
    lib: {
      entry: path.resolve(__dirname, './src/lib/index.tsx'),
      name: 'react-changes',
      fileName: (format) => `react-changes.${format}.js`
    },
    rollupOptions: {
      external: ['react', 'react-dom'],
      output: {
        globals: {
          react: 'React'
        }
      }
     
    },
  },
});
